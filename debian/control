Source: system-image
Maintainer: UBports Developers <developers@ubports.com>
Homepage: https://gitlab.com/ubports/core/system-image
Section: python
X-Python3-Version: >= 3.5
Priority: optional
Build-Depends: dbus,
               dbus-x11,
               debhelper-compat (= 13),
               dh-python,
               python3-all (>= 3.3),
               python3-dbus,
               python3-dbusmock,
               python3-docutils,
               python3-gi,
               python3-gnupg,
               python3-nose2,
               python3-pkg-resources,
               python3-psutil,
               python3-pycurl,
               python3-setuptools,
               python3-xdg,
               lomiri-download-manager,
Standards-Version: 3.9.7
Testsuite: autopkgtest
Vcs-Git: https://gitlab.com/ubports/core/system-image.git
Vcs-Browser: https://gitlab.com/ubports/core/system-image

Package: system-image-cli
Architecture: all
Depends: system-image-common (= ${binary:Version}),
         ${misc:Depends},
         ${python3:Depends},
Description: System image updater command line client
 This is the command line client for the system image updater.

Package: system-image-dbus
Architecture: all
Depends: system-image-common (= ${binary:Version}),
         ${misc:Depends},
         ${python3:Depends},
Description: System image updater command line client
 This is the command line client for the system image updater.

Package: system-image-common
Architecture: all
Depends: python3-dbus,
         python3-gi,
         python3-gnupg,
         python3-pkg-resources,
         python3-xdg,
         lomiri-download-manager | python3-pycurl,
         ${misc:Depends},
         ${python3:Depends},
Suggests: afirmflasher,
Description: System image updater
 This is the common bits for the system image updater.

Package: system-image-dev
Architecture: all
Depends: python3-gnupg,
         python3-psutil,
         ${misc:Depends},
         ${python3:Depends},
Description: System image updater development
 This is the development bits for the system image updater.
 Install this package if you want to run the tests.
