#!/bin/sh -euf

# Copyright (C) 2024 UBports Foundation.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This script generates cert.pem and nasty_cert.pem with the correct CommonName
# and with 10-years expiry. If tests fail with "FileNotFoundError: server
# certificate verification failed.", the certificate might have expired and this
# script would then have to be run again.
#
# Note that expired_cert.pem doesn't have to be re-generated as it's
# intentionally expired.

cd "$(dirname "$0")"
rm -v cert.pem key.pem nasty_cert.pem nasty_key.pem

# cert.pem has the correct CommonName (localhost).
openssl req -x509 \
    -nodes \
    -days 3650 \
    -newkey rsa:2048 \
    -subj "/C=US/ST=Some-State/O=Image Based Upgrades/CN=localhost" \
    -keyout key.pem \
    -out cert.pem

# nasty_cert.pem has the incorrect CommonName (www.example.com). This is used
# by TestHTTPSDownloadsNasty in test_download.py.
openssl req -x509 \
    -nodes \
    -days 3650 \
    -newkey rsa:2048 \
    -subj "/C=US/ST=Some-State/O=Image Based Upgrades/CN=www.example.com" \
    -keyout nasty_key.pem \
    -out nasty_cert.pem


